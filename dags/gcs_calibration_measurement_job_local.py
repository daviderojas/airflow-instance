from pyspark.sql import SparkSession


def main(*args):
    
    # GCS source path.
    gcs_path = "/home/david/Desktop/dboCalibrationMeasurement"

    # Bigquery destination table_id.
    target_table_id = "kpi-process:phoenix_csv.calibration_measurement_local"
    
    # Use the Cloud Storage bucket for temporary BigQuery export data used by the connector.
    bucket = "calibrations-dataproc-jobs"
    
    spark = SparkSession.builder \
        .appName("calibration_measurement_csv_etl") \
        .config("spark.jars.packages", "com.google.cloud.spark:spark-bigquery-with-dependencies_2.12:0.21.0") \
        .config("temporaryGcsBucket", bucket) \
        .getOrCreate()
    
    # Read csv data from GCS.
    df_measurement = spark.read \
        .option("escape", "\\") \
        .option("ignoreLeadingWhiteSpacestr", "false") \
        .option("ignoreTrailingWhiteSpacestr", "false") \
        .csv(gcs_path, header=True, inferSchema=False)

    df_nulos = df_measurement.where(df_measurement.CalibrationId.isNull())
    print(df_nulos.count())
    pass

    # Saving the data to BigQuery
    # df_measurement.write \
    #     .format("bigquery") \
    #     .option("table", target_table_id) \
    #     .mode("overwrite") \
    #     .save()


if __name__ == "__main__":
    main()
