from pyspark.sql import SparkSession


def main(*args):
    server_name = "jdbc:sqlserver://c0ek4d5zva.database.windows.net:1433"
    database_name = "calsystem_prd"
    url = server_name + ";" + "databaseName=" + database_name + ";"

    username = "s4b26jjdh8"
    password = "%5zOQZVBn066VOf*"
    table_name = "CalibrationMeasurement"

    spark = SparkSession.builder \
        .appName("mssql_calibration_measurement_spark") \
        .config("spark.jars.packages", "com.microsoft.azure:spark-mssql-connector_2.12_3.0:1.0.0-alpha") \
        .config("spark.jars", "/home/david/Desktop/mssql-jdbc-9.2.1.jre11.jar") \
        .getOrCreate()

    df = spark.read \
        .format("com.microsoft.sqlserver.jdbc.spark") \
        .option("url", url) \
        .option("dbtable", table_name) \
        .option("user", username) \
        .option("driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver") \
        .option("password", password).load()

    df.show()


if __name__ == "__main__":
    main()
