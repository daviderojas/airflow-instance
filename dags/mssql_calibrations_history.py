"""Example DAG demonstrating the usage of the PythonOperator."""
import time
from pprint import pprint

import pandas
import pymssql
from airflow import DAG
from airflow.operators.python import PythonOperator, PythonVirtualenvOperator
from datetime import datetime

args = {
    "owner": "datakimia",
}

with DAG(
    dag_id="calibrations_mssql_history_etl",
    default_args=args,
    schedule_interval="@daily",
    start_date=datetime(2021, 5, 20)
) as dag:

    def read_data():
        # Instance a python db connection object- same form as psycopg2/python-mysql drivers also
        conn = pymssql.connect(
            server="c0ek4d5zva.database.windows.net",
            user="s4b26jjdh8",
            password="%5zOQZVBn066VOf*",
            database="calsystem_prd",
        )

        query = (
            "SELECT * "
            "FROM  History "
            "WHERE SubmitedDate BETWEEN '2021-05-22T00:00:00.00' AND '2021-05-22T23:59:59.999'"
        )
        df = pandas.read_sql(query, conn)
        pass


    read_from_history_table = PythonVirtualenvOperator(
        task_id="read_from_history_table",
        python_callable=read_data,
        requirements=["pymssql"],
        system_site_packages=False,
    )
