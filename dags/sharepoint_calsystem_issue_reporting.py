"""Example DAG demonstrating the usage of the PythonOperator."""
from airflow import DAG
from airflow.operators.python import PythonVirtualenvOperator
from datetime import datetime, timedelta


default_args = {
    "owner": "datakimia",
    "depends_on_past": False,
    "email": ["davidrojj@gmail.com"],
    "email_on_failure": True,
    "email_on_retry": False,
    "retries": 2,
    "retry_delay": timedelta(minutes=1)
}

with DAG(
    dag_id="sharepoint_calsystem_issue_reporting",
    default_args=default_args,
    schedule_interval="@daily",
    start_date=datetime(2021, 5, 24)
) as dag:

    def sharepoint_list_etl():
        import pandas
        from office365.runtime.auth.authentication_context import AuthenticationContext
        from office365.sharepoint.client_context import ClientContext

        sp_user = "tconti@phoenixcalibrationdr.com"
        sp_password = "Datakimia2021"
        sp_url_report = "https://phoenixcalibrationdr.sharepoint.com/it"
        sp_list_name = "CalSystem Issue Reporting"

        bq_project_id = "kpi-process"
        bq_destination_table = "phoenix_airflow.raw_sharepoint_calsystem_issue_reporting"

        ctx_auth = AuthenticationContext(sp_url_report)
        if ctx_auth.acquire_token_for_user(sp_user, sp_password):
            ctx = ClientContext(sp_url_report, ctx_auth)
            list_object = ctx.web.lists.get_by_title(sp_list_name)
            items = list_object.get_items()
            ctx.load(items)
            ctx.execute_query()

            df = pandas.DataFrame.from_records([item.properties for item in items])
            df.drop("Id", inplace=True, axis=1)

            df.to_gbq(
                project_id=bq_project_id,
                destination_table=bq_destination_table,
                if_exists="replace",
                progress_bar=False
            )

    sharepoint_list_etl = PythonVirtualenvOperator(
        task_id="raw_sharepoint_calsystem_issue_reporting",
        python_callable=sharepoint_list_etl,
        requirements=["pandas-gbq", "Office365-REST-Python-Client"],
        system_site_packages=False,
    )
