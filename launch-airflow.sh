# airflow needs a home, ~/airflow is the default,
# but you can lay foundation somewhere else if you prefer
# (optional)
export AIRFLOW_HOME="/home/david/airflow-instance/"

AIRFLOW_VERSION=2.1.0
PYTHON_VERSION="$(python --version | cut -d " " -f 2 | cut -d "." -f 1-2)"
# For example: 3.6
CONSTRAINT_URL="https://raw.githubusercontent.com/apache/airflow/constraints-${AIRFLOW_VERSION}/constraints-${PYTHON_VERSION}.txt"
# For example: https://raw.githubusercontent.com/apache/airflow/constraints-2.1.0/constraints-3.6.txt
pip install "apache-airflow==${AIRFLOW_VERSION}" --constraint "${CONSTRAINT_URL}"

# initialize the database
airflow db init

airflow users create \
    --username drojas \
    --firstname David \
    --lastname Rojas \
    --role Admin \
    --email demarojas89@gmail.com

# start the web server, default port is 8080
# airflow webserver --port 8080

# start the scheduler
# open a new terminal or else run webserver with ``-D`` option to run it as a daemon
# airflow scheduler

# visit localhost:8080 in the browser and use the admin account you just
# created to login. Enable the example_bash_operator dag in the home page

# Issue with airflow db init.
# https://github.com/apache/airflow/issues/9069
 CREATE DATABASE airflow CHARACTER SET UTF8mb3 COLLATE utf8_general_ci;
 CREATE USER 'airflow_user' IDENTIFIED BY 'airflow_pass';
 GRANT ALL PRIVILEGES ON airflow.* TO 'airflow_user';
 SET GLOBAL explicit_defaults_for_timestamp = 1;